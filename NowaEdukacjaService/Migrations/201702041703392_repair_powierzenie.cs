namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class repair_powierzenie : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Budyneks",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nazwa = c.String(nullable: false, maxLength: 50, fixedLength: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.CyklKsztalcenias",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        rok1 = c.Int(nullable: false),
                        rok2 = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.FormaZajecs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.GrupaKursows",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        kursKoncowy = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kurs", t => t.kursKoncowy, cascadeDelete: true)
                .Index(t => t.kursKoncowy);
            
            CreateTable(
                "dbo.Kurs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        egzamin = c.Boolean(nullable: false),
                        kod = c.String(nullable: false, maxLength: 20, fixedLength: false),
                        liczbaGodzinCNPS = c.Int(nullable: false),
                        liczbaGodzinZZU = c.Int(nullable: false),
                        planowanaLiczbaGrup = c.Int(nullable: false),
                        przedmiot = c.Int(nullable: false),
                        formaZajec = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.FormaZajecs", t => t.formaZajec, cascadeDelete: true)
                .ForeignKey("dbo.Przedmiots", t => t.przedmiot, cascadeDelete: true)
                .Index(t => t.przedmiot)
                .Index(t => t.formaZajec);
            
            CreateTable(
                "dbo.Przedmiots",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nazwa = c.String(maxLength: 120, fixedLength: false),
                        semestr = c.Int(nullable: false),
                        jednostkaOrganizacyjna = c.Int(nullable: false),
                        planStudiow = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.JednostkaOrganizacyjnas", t => t.jednostkaOrganizacyjna, cascadeDelete: true)
                .ForeignKey("dbo.PlanStudiows", t => t.planStudiow, cascadeDelete: true)
                .ForeignKey("dbo.Semestrs", t => t.semestr, cascadeDelete: true)
                .Index(t => t.semestr)
                .Index(t => t.jednostkaOrganizacyjna)
                .Index(t => t.planStudiow);
            
            CreateTable(
                "dbo.JednostkaOrganizacyjnas",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        skrot = c.String(maxLength: 20, fixedLength: false),
                        nazwa = c.String(maxLength: 120, fixedLength: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.PlanStudiows",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        stopien = c.Int(nullable: false),
                        tryb = c.Int(nullable: false),
                        jezyk = c.Int(nullable: false),
                        kierunek = c.String(nullable: false, maxLength: 50, fixedLength: false),
                        cyklKsztalcenia = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.CyklKsztalcenias", t => t.cyklKsztalcenia, cascadeDelete: true)
                .Index(t => t.cyklKsztalcenia);
            
            CreateTable(
                "dbo.Semestrs",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        rok = c.Int(nullable: false),
                        typ = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.GrupaZajeciowas",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        kod = c.String(nullable: false, maxLength: 15, fixedLength: false),
                        kurs = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kurs", t => t.kurs, cascadeDelete: true)
                .Index(t => t.kurs);
            
            CreateTable(
                "dbo.Kurs_Powierzenie_PracownikDydaktyczny",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        kurs = c.Int(nullable: false),
                        powierzenie = c.Int(nullable: false),
                        pracownikDydaktyczny = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kurs", t => t.kurs, cascadeDelete: true)
                .ForeignKey("dbo.Powierzenies", t => t.powierzenie, cascadeDelete: true)
                .ForeignKey("dbo.Pracowniks", t => t.pracownikDydaktyczny, cascadeDelete: true)
                .Index(t => t.kurs)
                .Index(t => t.powierzenie)
                .Index(t => t.pracownikDydaktyczny);
            
            CreateTable(
                "dbo.Powierzenies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        zaakceptowane = c.Boolean(nullable: false),
                        liczbaGrupProponowana = c.Int(nullable: false),
                        liczbaGrupZaakceptowana = c.Int(nullable: false),
                        komentarz = c.String(maxLength: 255, fixedLength: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Pracowniks",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        imie = c.String(nullable: false, maxLength: 30, fixedLength: false),
                        nazwisko = c.String(nullable: false, maxLength: 40, fixedLength: false),
                        tytulyNaukowe = c.String(nullable: false, maxLength: 40, fixedLength: false),
                        login = c.String(nullable: false, maxLength: 20, fixedLength: false),
                        haslo = c.String(nullable: false, maxLength: 20, fixedLength: false),
                        stanowisko = c.Int(nullable: false),
                        obnizeniePensum = c.Int(),
                        pensum = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Stanowiskoes", t => t.stanowisko, cascadeDelete: true)
                .Index(t => t.stanowisko);
            
            CreateTable(
                "dbo.Stanowiskoes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nazwa = c.String(nullable: false, maxLength: 80, fixedLength: false),
                        pensum = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Kurs_WyposazenieSali",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        kurs = c.Int(nullable: false),
                        wyposazenieSali = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Kurs", t => t.kurs, cascadeDelete: true)
                .ForeignKey("dbo.WyposazenieSalis", t => t.wyposazenieSali, cascadeDelete: true)
                .Index(t => t.kurs)
                .Index(t => t.wyposazenieSali);
            
            CreateTable(
                "dbo.WyposazenieSalis",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nazwa = c.String(nullable: false, maxLength: 50, fixedLength: false),
                        identyfikator = c.String(nullable: false, maxLength: 50, fixedLength: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.KursCzastkowy_GrupaKursow",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        grupaKursow = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.GrupaKursows", t => t.grupaKursow, cascadeDelete: true)
                .Index(t => t.grupaKursow);
            
            CreateTable(
                "dbo.Powiadomienies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        dataWyslania = c.DateTime(nullable: false),
                        temat = c.String(maxLength: 100, fixedLength: false),
                        tresc = c.String(maxLength: 255, fixedLength: false),
                        nadawca = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Pracowniks", t => t.nadawca, cascadeDelete: true)
                .Index(t => t.nadawca);
            
            CreateTable(
                "dbo.Pracownik_Powiadomienie",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        powiadomienie = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Powiadomienies", t => t.powiadomienie, cascadeDelete: true)
                .Index(t => t.powiadomienie);
            
            CreateTable(
                "dbo.Pracownik_WyposazenieSali",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        pracownik = c.Int(nullable: false),
                        wyposazenieSali = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Pracowniks", t => t.pracownik, cascadeDelete: true)
                .ForeignKey("dbo.WyposazenieSalis", t => t.wyposazenieSali, cascadeDelete: true)
                .Index(t => t.pracownik)
                .Index(t => t.wyposazenieSali);
            
            CreateTable(
                "dbo.Raports",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nazwa = c.String(nullable: false, maxLength: 50, fixedLength: false),
                        tresc = c.String(nullable: false, maxLength: 50, fixedLength: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Raport_Powiadomienie",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        raport = c.Int(nullable: false),
                        powiadomienie = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Powiadomienies", t => t.powiadomienie, cascadeDelete: true)
                .ForeignKey("dbo.Raports", t => t.raport, cascadeDelete: true)
                .Index(t => t.raport)
                .Index(t => t.powiadomienie);
            
            CreateTable(
                "dbo.Salas",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        numer = c.String(nullable: false, maxLength: 6, fixedLength: false),
                        typ = c.Int(nullable: false),
                        liczbaMiejsc = c.Int(nullable: false),
                        budynek = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Budyneks", t => t.budynek, cascadeDelete: true)
                .Index(t => t.budynek);
            
            CreateTable(
                "dbo.Sala_WyposazenieSali",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        sala = c.Int(nullable: false),
                        wyposazenieSali = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Salas", t => t.sala, cascadeDelete: true)
                .ForeignKey("dbo.WyposazenieSalis", t => t.wyposazenieSali, cascadeDelete: true)
                .Index(t => t.sala)
                .Index(t => t.wyposazenieSali);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        indeks = c.String(nullable: false, maxLength: 10, fixedLength: false),
                        imie = c.String(nullable: false, maxLength: 20, fixedLength: false),
                        nazwisko = c.String(nullable: false, maxLength: 30, fixedLength: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        pracownikDydaktyczny = c.Int(nullable: false),
                        zainteresowaniaAkademickie = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Pracowniks", t => t.pracownikDydaktyczny, cascadeDelete: true)
                .Index(t => t.pracownikDydaktyczny);
            
            CreateTable(
                "dbo.Zajecias",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        eLearning = c.Boolean(nullable: false),
                        godzinaStart = c.DateTime(nullable: false),
                        godzinaStop = c.DateTime(nullable: false),
                        sala = c.Int(nullable: false),
                        grupaZajeciowa = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.GrupaZajeciowas", t => t.grupaZajeciowa, cascadeDelete: true)
                .ForeignKey("dbo.Salas", t => t.sala, cascadeDelete: true)
                .Index(t => t.sala)
                .Index(t => t.grupaZajeciowa);
            
            CreateTable(
                "dbo.Zapisies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nazwa = c.String(nullable: false, maxLength: 80, fixedLength: false),
                        planStudiow = c.Int(nullable: false),
                        semestr = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.PlanStudiows", t => t.planStudiow, cascadeDelete: true)
                .ForeignKey("dbo.Semestrs", t => t.semestr, cascadeDelete: true)
                .Index(t => t.planStudiow)
                .Index(t => t.semestr);
            
            CreateTable(
                "dbo.Zapisy_GrupaZajeciowa",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        grupaZajeciowa = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.GrupaZajeciowas", t => t.grupaZajeciowa, cascadeDelete: true)
                .Index(t => t.grupaZajeciowa);
            
            CreateTable(
                "dbo.Zapisy_Student",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        zapisy = c.Int(nullable: false),
                        student = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Students", t => t.student, cascadeDelete: true)
                .ForeignKey("dbo.Zapisies", t => t.zapisy, cascadeDelete: true)
                .Index(t => t.zapisy)
                .Index(t => t.student);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Zapisy_Student", "zapisy", "dbo.Zapisies");
            DropForeignKey("dbo.Zapisy_Student", "student", "dbo.Students");
            DropForeignKey("dbo.Zapisy_GrupaZajeciowa", "grupaZajeciowa", "dbo.GrupaZajeciowas");
            DropForeignKey("dbo.Zapisies", "semestr", "dbo.Semestrs");
            DropForeignKey("dbo.Zapisies", "planStudiow", "dbo.PlanStudiows");
            DropForeignKey("dbo.Zajecias", "sala", "dbo.Salas");
            DropForeignKey("dbo.Zajecias", "grupaZajeciowa", "dbo.GrupaZajeciowas");
            DropForeignKey("dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny", "pracownikDydaktyczny", "dbo.Pracowniks");
            DropForeignKey("dbo.Sala_WyposazenieSali", "wyposazenieSali", "dbo.WyposazenieSalis");
            DropForeignKey("dbo.Sala_WyposazenieSali", "sala", "dbo.Salas");
            DropForeignKey("dbo.Salas", "budynek", "dbo.Budyneks");
            DropForeignKey("dbo.Raport_Powiadomienie", "raport", "dbo.Raports");
            DropForeignKey("dbo.Raport_Powiadomienie", "powiadomienie", "dbo.Powiadomienies");
            DropForeignKey("dbo.Pracownik_WyposazenieSali", "wyposazenieSali", "dbo.WyposazenieSalis");
            DropForeignKey("dbo.Pracownik_WyposazenieSali", "pracownik", "dbo.Pracowniks");
            DropForeignKey("dbo.Pracownik_Powiadomienie", "powiadomienie", "dbo.Powiadomienies");
            DropForeignKey("dbo.Powiadomienies", "nadawca", "dbo.Pracowniks");
            DropForeignKey("dbo.Pracowniks", "stanowisko", "dbo.Stanowiskoes");
            DropForeignKey("dbo.KursCzastkowy_GrupaKursow", "grupaKursow", "dbo.GrupaKursows");
            DropForeignKey("dbo.Kurs_WyposazenieSali", "wyposazenieSali", "dbo.WyposazenieSalis");
            DropForeignKey("dbo.Kurs_WyposazenieSali", "kurs", "dbo.Kurs");
            DropForeignKey("dbo.Kurs_Powierzenie_PracownikDydaktyczny", "pracownikDydaktyczny", "dbo.Pracowniks");
            DropForeignKey("dbo.Kurs_Powierzenie_PracownikDydaktyczny", "powierzenie", "dbo.Powierzenies");
            DropForeignKey("dbo.Kurs_Powierzenie_PracownikDydaktyczny", "kurs", "dbo.Kurs");
            DropForeignKey("dbo.GrupaZajeciowas", "kurs", "dbo.Kurs");
            DropForeignKey("dbo.GrupaKursows", "kursKoncowy", "dbo.Kurs");
            DropForeignKey("dbo.Kurs", "przedmiot", "dbo.Przedmiots");
            DropForeignKey("dbo.Przedmiots", "semestr", "dbo.Semestrs");
            DropForeignKey("dbo.Przedmiots", "planStudiow", "dbo.PlanStudiows");
            DropForeignKey("dbo.PlanStudiows", "cyklKsztalcenia", "dbo.CyklKsztalcenias");
            DropForeignKey("dbo.Przedmiots", "jednostkaOrganizacyjna", "dbo.JednostkaOrganizacyjnas");
            DropForeignKey("dbo.Kurs", "formaZajec", "dbo.FormaZajecs");
            DropIndex("dbo.Zapisy_Student", new[] { "student" });
            DropIndex("dbo.Zapisy_Student", new[] { "zapisy" });
            DropIndex("dbo.Zapisy_GrupaZajeciowa", new[] { "grupaZajeciowa" });
            DropIndex("dbo.Zapisies", new[] { "semestr" });
            DropIndex("dbo.Zapisies", new[] { "planStudiow" });
            DropIndex("dbo.Zajecias", new[] { "grupaZajeciowa" });
            DropIndex("dbo.Zajecias", new[] { "sala" });
            DropIndex("dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny", new[] { "pracownikDydaktyczny" });
            DropIndex("dbo.Sala_WyposazenieSali", new[] { "wyposazenieSali" });
            DropIndex("dbo.Sala_WyposazenieSali", new[] { "sala" });
            DropIndex("dbo.Salas", new[] { "budynek" });
            DropIndex("dbo.Raport_Powiadomienie", new[] { "powiadomienie" });
            DropIndex("dbo.Raport_Powiadomienie", new[] { "raport" });
            DropIndex("dbo.Pracownik_WyposazenieSali", new[] { "wyposazenieSali" });
            DropIndex("dbo.Pracownik_WyposazenieSali", new[] { "pracownik" });
            DropIndex("dbo.Pracownik_Powiadomienie", new[] { "powiadomienie" });
            DropIndex("dbo.Powiadomienies", new[] { "nadawca" });
            DropIndex("dbo.KursCzastkowy_GrupaKursow", new[] { "grupaKursow" });
            DropIndex("dbo.Kurs_WyposazenieSali", new[] { "wyposazenieSali" });
            DropIndex("dbo.Kurs_WyposazenieSali", new[] { "kurs" });
            DropIndex("dbo.Pracowniks", new[] { "stanowisko" });
            DropIndex("dbo.Kurs_Powierzenie_PracownikDydaktyczny", new[] { "pracownikDydaktyczny" });
            DropIndex("dbo.Kurs_Powierzenie_PracownikDydaktyczny", new[] { "powierzenie" });
            DropIndex("dbo.Kurs_Powierzenie_PracownikDydaktyczny", new[] { "kurs" });
            DropIndex("dbo.GrupaZajeciowas", new[] { "kurs" });
            DropIndex("dbo.PlanStudiows", new[] { "cyklKsztalcenia" });
            DropIndex("dbo.Przedmiots", new[] { "planStudiow" });
            DropIndex("dbo.Przedmiots", new[] { "jednostkaOrganizacyjna" });
            DropIndex("dbo.Przedmiots", new[] { "semestr" });
            DropIndex("dbo.Kurs", new[] { "formaZajec" });
            DropIndex("dbo.Kurs", new[] { "przedmiot" });
            DropIndex("dbo.GrupaKursows", new[] { "kursKoncowy" });
            DropTable("dbo.Zapisy_Student");
            DropTable("dbo.Zapisy_GrupaZajeciowa");
            DropTable("dbo.Zapisies");
            DropTable("dbo.Zajecias");
            DropTable("dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny");
            DropTable("dbo.Students");
            DropTable("dbo.Sala_WyposazenieSali");
            DropTable("dbo.Salas");
            DropTable("dbo.Raport_Powiadomienie");
            DropTable("dbo.Raports");
            DropTable("dbo.Pracownik_WyposazenieSali");
            DropTable("dbo.Pracownik_Powiadomienie");
            DropTable("dbo.Powiadomienies");
            DropTable("dbo.KursCzastkowy_GrupaKursow");
            DropTable("dbo.WyposazenieSalis");
            DropTable("dbo.Kurs_WyposazenieSali");
            DropTable("dbo.Stanowiskoes");
            DropTable("dbo.Pracowniks");
            DropTable("dbo.Powierzenies");
            DropTable("dbo.Kurs_Powierzenie_PracownikDydaktyczny");
            DropTable("dbo.GrupaZajeciowas");
            DropTable("dbo.Semestrs");
            DropTable("dbo.PlanStudiows");
            DropTable("dbo.JednostkaOrganizacyjnas");
            DropTable("dbo.Przedmiots");
            DropTable("dbo.Kurs");
            DropTable("dbo.GrupaKursows");
            DropTable("dbo.FormaZajecs");
            DropTable("dbo.CyklKsztalcenias");
            DropTable("dbo.Budyneks");
        }
    }
}
