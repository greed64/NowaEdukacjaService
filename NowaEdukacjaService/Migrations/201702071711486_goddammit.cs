namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class goddammit : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Budyneks", "nazwa", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Kurs", "kod", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Przedmiots", "nazwa", c => c.String(maxLength: 120));
            AlterColumn("dbo.JednostkaOrganizacyjnas", "skrot", c => c.String(maxLength: 20));
            AlterColumn("dbo.JednostkaOrganizacyjnas", "nazwa", c => c.String(maxLength: 120));
            AlterColumn("dbo.PlanStudiows", "kierunek", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.GrupaZajeciowas", "kod", c => c.String(nullable: false, maxLength: 15));
            AlterColumn("dbo.Powierzenies", "komentarz", c => c.String(maxLength: 255));
            AlterColumn("dbo.Pracowniks", "imie", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Pracowniks", "nazwisko", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.Pracowniks", "tytulyNaukowe", c => c.String(nullable: false, maxLength: 40));
            AlterColumn("dbo.Pracowniks", "login", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Pracowniks", "haslo", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Stanowiskoes", "nazwa", c => c.String(nullable: false, maxLength: 80));
            AlterColumn("dbo.WyposazenieSalis", "nazwa", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.WyposazenieSalis", "identyfikator", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Powiadomienies", "temat", c => c.String(maxLength: 100));
            AlterColumn("dbo.Powiadomienies", "tresc", c => c.String(maxLength: 255));
            AlterColumn("dbo.Raports", "nazwa", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Raports", "tresc", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Salas", "numer", c => c.String(nullable: false, maxLength: 6));
            AlterColumn("dbo.Students", "indeks", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Students", "imie", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Students", "nazwisko", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.Zapisies", "nazwa", c => c.String(nullable: false, maxLength: 80));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Zapisies", "nazwa", c => c.String(nullable: false, maxLength: 80, fixedLength: true));
            AlterColumn("dbo.Students", "nazwisko", c => c.String(nullable: false, maxLength: 30, fixedLength: true));
            AlterColumn("dbo.Students", "imie", c => c.String(nullable: false, maxLength: 20, fixedLength: true));
            AlterColumn("dbo.Students", "indeks", c => c.String(nullable: false, maxLength: 10, fixedLength: true));
            AlterColumn("dbo.Salas", "numer", c => c.String(nullable: false, maxLength: 6, fixedLength: true));
            AlterColumn("dbo.Raports", "tresc", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.Raports", "nazwa", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.Powiadomienies", "tresc", c => c.String(maxLength: 255, fixedLength: true));
            AlterColumn("dbo.Powiadomienies", "temat", c => c.String(maxLength: 100, fixedLength: true));
            AlterColumn("dbo.WyposazenieSalis", "identyfikator", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.WyposazenieSalis", "nazwa", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.Stanowiskoes", "nazwa", c => c.String(nullable: false, maxLength: 80, fixedLength: true));
            AlterColumn("dbo.Pracowniks", "haslo", c => c.String(nullable: false, maxLength: 20, fixedLength: true));
            AlterColumn("dbo.Pracowniks", "login", c => c.String(nullable: false, maxLength: 20, fixedLength: true));
            AlterColumn("dbo.Pracowniks", "tytulyNaukowe", c => c.String(nullable: false, maxLength: 40, fixedLength: true));
            AlterColumn("dbo.Pracowniks", "nazwisko", c => c.String(nullable: false, maxLength: 40, fixedLength: true));
            AlterColumn("dbo.Pracowniks", "imie", c => c.String(nullable: false, maxLength: 30, fixedLength: true));
            AlterColumn("dbo.Powierzenies", "komentarz", c => c.String(maxLength: 255, fixedLength: true));
            AlterColumn("dbo.GrupaZajeciowas", "kod", c => c.String(nullable: false, maxLength: 15, fixedLength: true));
            AlterColumn("dbo.PlanStudiows", "kierunek", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
            AlterColumn("dbo.JednostkaOrganizacyjnas", "nazwa", c => c.String(maxLength: 120, fixedLength: true));
            AlterColumn("dbo.JednostkaOrganizacyjnas", "skrot", c => c.String(maxLength: 20, fixedLength: true));
            AlterColumn("dbo.Przedmiots", "nazwa", c => c.String(maxLength: 120, fixedLength: true));
            AlterColumn("dbo.Kurs", "kod", c => c.String(nullable: false, maxLength: 20, fixedLength: true));
            AlterColumn("dbo.Budyneks", "nazwa", c => c.String(nullable: false, maxLength: 50, fixedLength: true));
        }
    }
}
