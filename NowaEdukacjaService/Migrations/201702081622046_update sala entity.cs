namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatesalaentity : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Salas", new[] { "budynek" });
            CreateIndex("dbo.Salas", new[] { "numer", "budynek" }, unique: true, name: "NumerAndBudynek");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Salas", "NumerAndBudynek");
            CreateIndex("dbo.Salas", "budynek");
        }
    }
}
