namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class jednostkaorgpracownik : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Pracowniks", "jednostkaOrganizacyjna", c => c.Int());
            CreateIndex("dbo.Pracowniks", "jednostkaOrganizacyjna");
            AddForeignKey("dbo.Pracowniks", "jednostkaOrganizacyjna", "dbo.JednostkaOrganizacyjnas", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pracowniks", "jednostkaOrganizacyjna", "dbo.JednostkaOrganizacyjnas");
            DropIndex("dbo.Pracowniks", new[] { "jednostkaOrganizacyjna" });
            DropColumn("dbo.Pracowniks", "jednostkaOrganizacyjna");
        }
    }
}
