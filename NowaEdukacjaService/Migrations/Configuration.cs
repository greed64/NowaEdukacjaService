﻿namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Model.Concrete;

    //internal sealed class Configuration : DbMigrationsConfiguration<NowaEdukacjaService.Model.Concrete.NowaEdukacjaContext>
    //{
    //    public Configuration()
    //    {
    //        AutomaticMigrationsEnabled = false;
    //    }

    //    protected override void Seed(NowaEdukacjaService.Model.Concrete.NowaEdukacjaContext context)
    //    {

    internal sealed class Configuration : DbMigrationsConfiguration<NowaEdukacjaContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(NowaEdukacjaContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            /*
            if (System.Diagnostics.Debugger.IsAttached == false)
            {
                System.Diagnostics.Debugger.Launch();
            }//*/

            try
            {
                context.Stanowisko.AddOrUpdate(
                    s => s.Id,
                    new Stanowisko { Id = 1, Nazwa = "profesor zwyczajny", Pensum = 210 },
                    new Stanowisko { Id = 2, Nazwa = "adiunkt", Pensum = 240 },
                    new Stanowisko { Id = 3, Nazwa = "docent", Pensum = 300 },
                    new Stanowisko { Id = 4, Nazwa = "wykładowca", Pensum = 360 },
                    new Stanowisko { Id = 5, Nazwa = "lektor", Pensum = 540 }
                );

                context.PelnomocnikDziekanaDoSprawKierunku.AddOrUpdate(
                    p => p.Id,
                    new PelnomocnikDziekanaDoSprawKierunku { Id = 1, IdStanowisko = 1, Imie = "Zyta", Nazwisko = "Berło", TytulyNaukowe = "dr" }
                );

                context.Budynek.AddOrUpdate(
                    b => b.Id,
                    new Budynek { Id = 1, Nazwa = "C-13" },
                    new Budynek { Id = 2, Nazwa = "D-2" }
                );

                context.WyposazenieSali.AddOrUpdate(
                    w => w.Id,
                    new WyposazenieSali { Id = 1, Nazwa = "tablica kredowa", Identyfikator = "kred0001" },
                    new WyposazenieSali { Id = 2, Nazwa = "tablica biała", Identyfikator = "mazak0001" },
                    new WyposazenieSali { Id = 3, Nazwa = "projektor VGA", Identyfikator = "vga0001" },
                    new WyposazenieSali { Id = 4, Nazwa = "projektor HDMI", Identyfikator = "hdmi0001" },
                    new WyposazenieSali { Id = 5, Nazwa = "naświetlacz przeźroczy", Identyfikator = "nas0001" }
                );

                context.Sala.AddOrUpdate(
                    s => s.Id,
                    new Sala { Id = 1, IdBudynek = 2, Numer = "107a", LiczbaMiejsc = 18, Typ = TypSali.LABORATORIUM_KOMPUTEROWE },
                    new Sala { Id = 2, IdBudynek = 1, Numer = "1.30", LiczbaMiejsc = 250, Typ = TypSali.WYKLADOWA },
                    new Sala { Id = 3, IdBudynek = 2, Numer = "201.20", LiczbaMiejsc = 13, Typ = TypSali.CWICZENIOWA }
                );

                context.Sala_WyposazenieSali.AddOrUpdate(
                    sw => sw.Id,
                    new Sala_WyposazenieSali { Id = 1, IdSala = 1, IdWyposazenieSali = 2 },
                    new Sala_WyposazenieSali { Id = 2, IdSala = 1, IdWyposazenieSali = 3 },
                    new Sala_WyposazenieSali { Id = 3, IdSala = 1, IdWyposazenieSali = 4 },
                    new Sala_WyposazenieSali { Id = 4, IdSala = 2, IdWyposazenieSali = 3 },
                    new Sala_WyposazenieSali { Id = 5, IdSala = 2, IdWyposazenieSali = 4 },
                    new Sala_WyposazenieSali { Id = 6, IdSala = 3, IdWyposazenieSali = 1 },
                    new Sala_WyposazenieSali { Id = 7, IdSala = 3, IdWyposazenieSali = 2 },
                    new Sala_WyposazenieSali { Id = 8, IdSala = 3, IdWyposazenieSali = 3 },
                    new Sala_WyposazenieSali { Id = 9, IdSala = 3, IdWyposazenieSali = 4 }
                );

                context.JednostkaOrganizacyjna.AddOrUpdate(
                    j => j.Id,
                    new JednostkaOrganizacyjna { Id = 1, Nazwa = "Katedra Systemów Informatycznych", Skrot = "W8/K1" },
                    new JednostkaOrganizacyjna { Id = 2, Nazwa = "Katedra Inteligencji Obliczeniowej", Skrot = "W8/K2" }
                );

                context.PracownikDydaktyczny.AddOrUpdate(
                    p => p.Id,
                    new PracownikDydaktyczny { Id = 2, IdStanowisko = 1, Imie = "Jan", Nazwisko = "Nestor", TytulyNaukowe = "prof.", ObnizeniePensum = 0, IdJednostkaOrganizacyjna = 2 },
                    new PracownikDydaktyczny { Id = 3, IdStanowisko = 2, Imie = "Izabela", Nazwisko = "Adiunktowska", TytulyNaukowe = "dr", ObnizeniePensum = 0 },
                    new PracownikDydaktyczny { Id = 4, IdStanowisko = 3, Imie = "Joachim", Nazwisko = "Duszyński", TytulyNaukowe = "dr", ObnizeniePensum = 0, IdJednostkaOrganizacyjna = 1 },
                    new PracownikDydaktyczny { Id = 5, IdStanowisko = 4, Imie = "Beata", Nazwisko = "Ojdana", TytulyNaukowe = "dr", ObnizeniePensum = 0 },
                    new PracownikDydaktyczny { Id = 6, IdStanowisko = 4, Imie = "Beata", Nazwisko = "Antczak", TytulyNaukowe = "dr", ObnizeniePensum = 0 }
                );

                context.CyklKsztalcenia.AddOrUpdate(
                    c => c.Id,
                    new CyklKsztalcenia { Id = 1, Rok1 = 2017, Rok2 = 2018 }
                );

                context.Semestr.AddOrUpdate(
                    s => s.Id,
                    new Semestr { Id = 1, Rok = 2017, Typ = TypSemestru.ZIMOWY },
                    new Semestr { Id = 2, Rok = 2018, Typ = TypSemestru.LETNI }
                );

                context.PlanStudiow.AddOrUpdate(
                    p => p.Id,
                    new PlanStudiow { Id = 1, IdCyklKsztalcenia = 1, Jezyk = Jezyk.POLSKI, Tryb = Tryb.DZIENNE, Kierunek = "Informatyka", Stopien = 1 }
                );

                context.Przedmiot.AddOrUpdate(
                    p => p.Id,
                    new Przedmiot { Id = 1, IdJednostkaOrganizacyjna = 1, IdPlanStudiow = 1, IdSemestr = 1, nazwa = "Modele Predykcji i Metody w Inżynierii Oprogramowania" },
                    new Przedmiot { Id = 2, IdJednostkaOrganizacyjna = 2, IdPlanStudiow = 1, IdSemestr = 1, nazwa = "Techniki Perswazji z Buntowniczą Sztuczną Inteligencją" }
                );

                context.ZainteresowaniaAkademickie.AddOrUpdate(
                    z => z.Id,
                    new ZainteresowaniaAkademickie { Id = 1, Nazwa = "straszenie nieposłusznej SI magnesami" },
                    new ZainteresowaniaAkademickie { Id = 2, Nazwa = "reprodukowalność badań" },
                    new ZainteresowaniaAkademickie { Id = 3, Nazwa = "ezoteryczne specyfikacje" },
                    new ZainteresowaniaAkademickie { Id = 4, Nazwa = "inżynieria oprogramowania" },
                    new ZainteresowaniaAkademickie { Id = 5, Nazwa = "wpływ paradoksów na zdolności kognitywne SI" },
                    new ZainteresowaniaAkademickie { Id = 6, Nazwa = "algorytmy genetyczne" },
                    new ZainteresowaniaAkademickie { Id = 7, Nazwa = "metryki inżynierii oprogramowania" },
                    new ZainteresowaniaAkademickie { Id = 8, Nazwa = "gramatyki bezkontekstowe" },
                    new ZainteresowaniaAkademickie { Id = 9, Nazwa = "user experience" }
                );

                context.ZainteresowaniaAkademickie_PracownikDydaktyczny.AddOrUpdate(
                    za => za.Id,
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 1, IdPracownikDydaktyczny = 2, IdZainteresowaniaAkademickie = 1 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 2, IdPracownikDydaktyczny = 2, IdZainteresowaniaAkademickie = 3 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 3, IdPracownikDydaktyczny = 2, IdZainteresowaniaAkademickie = 4 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 4, IdPracownikDydaktyczny = 2, IdZainteresowaniaAkademickie = 5 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 5, IdPracownikDydaktyczny = 3, IdZainteresowaniaAkademickie = 2 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 6, IdPracownikDydaktyczny = 3, IdZainteresowaniaAkademickie = 3 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 7, IdPracownikDydaktyczny = 3, IdZainteresowaniaAkademickie = 7 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 8, IdPracownikDydaktyczny = 3, IdZainteresowaniaAkademickie = 9 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 9, IdPracownikDydaktyczny = 4, IdZainteresowaniaAkademickie = 2 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 10, IdPracownikDydaktyczny = 4, IdZainteresowaniaAkademickie = 4 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 11, IdPracownikDydaktyczny = 4, IdZainteresowaniaAkademickie = 7 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 12, IdPracownikDydaktyczny = 5, IdZainteresowaniaAkademickie = 3 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 13, IdPracownikDydaktyczny = 5, IdZainteresowaniaAkademickie = 8 },
                    new ZainteresowaniaAkademickie_PracownikDydaktyczny { Id = 14, IdPracownikDydaktyczny = 6, IdZainteresowaniaAkademickie = 9 }
                );

                context.FormaZajec.AddOrUpdate(
                    f => f.Id,
                    new FormaZajec { Id = 1, MinLiczbaMiejsc = 70, MaxLiczbaMiejsc = 200, nazwa = "wykład ogólny" },
                    new FormaZajec { Id = 2, MinLiczbaMiejsc = 30, MaxLiczbaMiejsc = 60, nazwa = "wykład kierunkowy" },
                    new FormaZajec { Id = 3, MinLiczbaMiejsc = 25, MaxLiczbaMiejsc = 50, nazwa = "ćwiczenia audytoryjne" },
                    new FormaZajec { Id = 4, MinLiczbaMiejsc = 25, MaxLiczbaMiejsc = 50, nazwa = "ćwiczenia kierunkowe" },
                    new FormaZajec { Id = 5, MinLiczbaMiejsc = 15, MaxLiczbaMiejsc = 30, nazwa = "seminaria" },
                    new FormaZajec { Id = 6, MinLiczbaMiejsc = 10, MaxLiczbaMiejsc = 20, nazwa = "zajęcia projektowe" },
                    new FormaZajec { Id = 7, MinLiczbaMiejsc = 10, MaxLiczbaMiejsc = 20, nazwa = "zajęcia laboratoryjne" },
                    new FormaZajec { Id = 8, MinLiczbaMiejsc = 10, MaxLiczbaMiejsc = 20, nazwa = "zajęcia badawcze" }
                );

                context.Kurs.AddOrUpdate(
                    k => k.Id,
                    new Kurs { Id = 1, IdPrzedmiot = 1, IdFormaZajec = 1, Egzamin = true, Kod = "INZ000001W", LiczbaGodzinZZU = 30, LiczbaGodzinCNPS = 90, PlanowanaLiczbaGrup = 2 },
                    new Kurs { Id = 2, IdPrzedmiot = 1, IdFormaZajec = 5, Egzamin = false, Kod = "INZ000001S", LiczbaGodzinZZU = 15, LiczbaGodzinCNPS = 90, PlanowanaLiczbaGrup = 20 },
                    new Kurs { Id = 3, IdPrzedmiot = 1, IdFormaZajec = 7, Egzamin = false, Kod = "INZ000001L", LiczbaGodzinZZU = 30, LiczbaGodzinCNPS = 90, PlanowanaLiczbaGrup = 30 },
                    new Kurs { Id = 4, IdPrzedmiot = 2, IdFormaZajec = 1, Egzamin = true, Kod = "INZ000002W", LiczbaGodzinZZU = 30, LiczbaGodzinCNPS = 90, PlanowanaLiczbaGrup = 2 },
                    new Kurs { Id = 5, IdPrzedmiot = 2, IdFormaZajec = 3, Egzamin = false, Kod = "INZ000002C", LiczbaGodzinZZU = 30, LiczbaGodzinCNPS = 90, PlanowanaLiczbaGrup = 15 },
                    new Kurs { Id = 6, IdPrzedmiot = 2, IdFormaZajec = 7, Egzamin = false, Kod = "INZ000002L", LiczbaGodzinZZU = 15, LiczbaGodzinCNPS = 90, PlanowanaLiczbaGrup = 30 }
                );

                context.Kurs_WyposazenieSali.AddOrUpdate(
                    kw => kw.Id,
                    new Kurs_WyposazenieSali { Id = 1, IdKurs = 1, IdWyposazenieSali = 1 },
                    new Kurs_WyposazenieSali { Id = 2, IdKurs = 1, IdWyposazenieSali = 4 },
                    new Kurs_WyposazenieSali { Id = 3, IdKurs = 2, IdWyposazenieSali = 4 },
                    new Kurs_WyposazenieSali { Id = 4, IdKurs = 5, IdWyposazenieSali = 2 }
                );

                context.Powierzenie.AddOrUpdate(
                    p => p.Id,
                    new Powierzenie { Id = 1, LiczbaGrupProponowana = 2, LiczbaGrupZaakceptowana = 0, Zaakceptowane = false, Komentarz = null },
                    new Powierzenie { Id = 2, LiczbaGrupProponowana = 2, LiczbaGrupZaakceptowana = 0, Zaakceptowane = false, Komentarz = null },
                    new Powierzenie { Id = 3, LiczbaGrupProponowana = 10, LiczbaGrupZaakceptowana = 0, Zaakceptowane = false, Komentarz = null },
                    new Powierzenie { Id = 4, LiczbaGrupProponowana = 15, LiczbaGrupZaakceptowana = 0, Zaakceptowane = false, Komentarz = null }
                );

                context.Kurs_Powierzenie_PracownikDydaktyczny.AddOrUpdate(
                    kpp => kpp.Id,
                    new Kurs_Powierzenie_PracownikDydaktyczny { Id = 1, IdKurs = 1, IdPowierzenie = 1, IdPracownikDydaktyczny = 4 },
                    new Kurs_Powierzenie_PracownikDydaktyczny { Id = 2, IdKurs = 4, IdPowierzenie = 2, IdPracownikDydaktyczny = 2 },
                    new Kurs_Powierzenie_PracownikDydaktyczny { Id = 3, IdKurs = 5, IdPowierzenie = 3, IdPracownikDydaktyczny = 2 },
                    new Kurs_Powierzenie_PracownikDydaktyczny { Id = 4, IdKurs = 6, IdPowierzenie = 4, IdPracownikDydaktyczny = 2 }
                );
                //*/
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    System.Diagnostics.Debug.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        System.Diagnostics.Debug.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }//Ten cały catch to właściwie chyba w ogóle nie działa.
        }
    }
}
