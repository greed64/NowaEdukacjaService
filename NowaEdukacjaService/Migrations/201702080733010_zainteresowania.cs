namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class zainteresowania : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ZainteresowaniaAkademickies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        nazwa = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateIndex("dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny", "zainteresowaniaAkademickie");
            AddForeignKey("dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny", "zainteresowaniaAkademickie", "dbo.ZainteresowaniaAkademickies", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny", "zainteresowaniaAkademickie", "dbo.ZainteresowaniaAkademickies");
            DropIndex("dbo.ZainteresowaniaAkademickie_PracownikDydaktyczny", new[] { "zainteresowaniaAkademickie" });
            DropTable("dbo.ZainteresowaniaAkademickies");
        }
    }
}
