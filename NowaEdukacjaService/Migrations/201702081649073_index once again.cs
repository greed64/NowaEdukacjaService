namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class indexonceagain : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Salas", new[] { "budynek" });
            CreateIndex("dbo.Salas", new[] { "numer", "budynek" }, unique: true, name: "IX_NumerAndBudynek");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Salas", "IX_NumerAndBudynek");
            CreateIndex("dbo.Salas", "budynek");
        }
    }
}
