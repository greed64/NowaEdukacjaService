// <auto-generated />
namespace NowaEdukacjaService.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class fixsale : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(fixsale));
        
        string IMigrationMetadata.Id
        {
            get { return "201702081643232_fix sale"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
