namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usuniecie_required_z_wyliczalnego1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Pracowniks", "login", c => c.String(maxLength: 20));
            AlterColumn("dbo.Pracowniks", "haslo", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Pracowniks", "haslo", c => c.String(nullable: false, maxLength: 20));
            AlterColumn("dbo.Pracowniks", "login", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
