namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class kluczobcybylniewinny : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.Pracowniks", "jednostkaOrganizacyjna");
            AddForeignKey("dbo.Pracowniks", "jednostkaOrganizacyjna", "dbo.JednostkaOrganizacyjnas", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pracowniks", "jednostkaOrganizacyjna", "dbo.JednostkaOrganizacyjnas");
            DropIndex("dbo.Pracowniks", new[] { "jednostkaOrganizacyjna" });
        }
    }
}
