namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class usunkluczobcy : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Pracowniks", "jednostkaOrganizacyjna", "dbo.JednostkaOrganizacyjnas");
            DropIndex("dbo.Pracowniks", new[] { "jednostkaOrganizacyjna" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Pracowniks", "jednostkaOrganizacyjna");
            AddForeignKey("dbo.Pracowniks", "jednostkaOrganizacyjna", "dbo.JednostkaOrganizacyjnas", "id");
        }
    }
}
