namespace NowaEdukacjaService.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class formazajecgetset : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormaZajecs", "nazwa", c => c.String(nullable: false, maxLength: 80));
            AddColumn("dbo.FormaZajecs", "minLiczbaMiejsc", c => c.Int(nullable: false));
            AddColumn("dbo.FormaZajecs", "maxLiczbaMiejsc", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FormaZajecs", "maxLiczbaMiejsc");
            DropColumn("dbo.FormaZajecs", "minLiczbaMiejsc");
            DropColumn("dbo.FormaZajecs", "nazwa");
        }
    }
}
