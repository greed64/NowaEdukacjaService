﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using NowaEdukacjaService.Model.Concrete;

namespace NowaEdukacjaService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NowaEdukacjaService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select NowaEdukacjaService.svc or NowaEdukacjaService.svc.cs at the Solution Explorer and start debugging.
    public class NowaEdukacjaService : INowaEdukacjaService
    {
        //NowaEdukacjaContext context = new NowaEdukacjaContext();
        Concrete<Budynek> concreteBudynek = new Concrete<Budynek>();

        public IQueryable<Budynek> getBudynek() {
            Budynek b1 = new Budynek();
            return concreteBudynek.interfaceRepository;
        }

        public void deleteBudynek(Budynek budynek) {
            concreteBudynek.Delete(budynek.Id);
        }

        public void addBudynek(Budynek budynek)
        {
            concreteBudynek.Save(budynek);
        }
    }
}
