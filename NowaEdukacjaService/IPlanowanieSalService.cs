﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NowaEdukacjaService {
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPlanowanieSalService" in both code and config file together.
    [ServiceContract]
    public interface IPlanowanieSalService {

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Sala> getAllClassrooms();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<WyposazenieSali> getAllEquipment();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<TypSali> getAllClassroomTypes();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Budynek> getAllBuildings();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<WyposazenieSali> getEquipmentByClassroom(int id);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        int addClassroom(Sala sala);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        int addEquipment(int classroomId, WyposazenieSali wyposazenieSali);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        int addBuilding(Budynek budynek);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        void removeClassroom(int id);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        void removeEquipmentFromClassroom(int classroomId, WyposazenieSali wyposazenieSali);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        void removeBuilding(int buildingId);

    }
}
