﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NowaEdukacjaService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IZatwierdzanieIRaportowaniePowierzenService" in both code and config file together.
    [ServiceContract]
    public interface IZatwierdzanieIRaportowaniePowierzenService
    {
        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<JednostkaOrganizacyjna> getAllOrgUnits();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<PracownikDydaktyczny> getEmployeesByNameInterestTitle(
            string firstName,
            string lastName,
            string interest,
            string title);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<PlanStudiow> getAllStudyPlans();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<CyklKsztalcenia> getAllEducationCycles();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Semestr> getAllSemesters();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Kurs> getCoursesByCyclePlanSemester(
            int EducationCycleId,
            int StudyPlanId,
            int SemesterId);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Przedmiot> getSubjectsByCourses(List<int> ids_from_courses);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Powierzenie> getCommittalsByCourses(List<int> ids_from_courses);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<PracownikDydaktyczny> getEmployeesByCommittals(List<int> ids_from_committals);

        /*[OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<ZainteresowaniaAkademickie> getInterestsByEmployees(List<int> ids_from_employees);*/

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<ZainteresowaniaAkademickie> getInterestsByEmployee(int id);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Stanowisko> getAllJobTitles();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        Kurs_Powierzenie_PracownikDydaktyczny getLinkByCommittal(int id);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<FormaZajec> getAllCourseForms();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        int addCommittal(int CourseId, int EmployeeId, int GroupCount);
    }
}
