﻿using NowaEdukacjaService.Model.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NowaEdukacjaService {
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PlanowanieSalService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PlanowanieSalService.svc or PlanowanieSalService.svc.cs at the Solution Explorer and start debugging.
    public class PlanowanieSalService : IPlanowanieSalService {

        public int addBuilding(Budynek budynek) {
            Concrete<Budynek> concreteBudynek = new Concrete<Budynek>();
            return concreteBudynek.Save(budynek);
        }

        public int addClassroom(Sala sala) {
            try {

                Concrete<Sala> concreteSala = new Concrete<Sala>();
                return concreteSala.Save(sala);
            } catch (Exception)
            {
                var mf = new Fault();
                mf.Operation = "Błąd podczas operacji: Dodaj salę";
                mf.Message = "Sala o wskazanym numerze, dla wybranego budynku już istnieje";
                throw new FaultException<Fault>(mf);
            } 
        }


        public int addEquipment(int classroomId, WyposazenieSali wyposazenieSali) {
            Concrete<WyposazenieSali> concreteWyposazenieSali = new Concrete<WyposazenieSali>();
            Concrete<Sala_WyposazenieSali> concreteS_WS = new Concrete<Sala_WyposazenieSali>();
            int wyposazenieSaliId = concreteWyposazenieSali
                .Save(wyposazenieSali);
            concreteS_WS
                .Save(new Sala_WyposazenieSali() {
                    IdSala = classroomId,
                    IdWyposazenieSali = wyposazenieSaliId
                });
            return wyposazenieSaliId;
        }

        public IQueryable<Budynek> getAllBuildings() {
            Concrete<Budynek> concreteBudynek = new Concrete<Budynek>();
            return concreteBudynek
                .interfaceRepository;
        }

        public IQueryable<Sala> getAllClassrooms() {
            Concrete<Sala> concreteSala = new Concrete<Sala>();
            return concreteSala
                .interfaceRepository;
        }

        public IQueryable<TypSali> getAllClassroomTypes() {
            return ((TypSali[]) Enum.GetValues(typeof(TypSali))).AsQueryable();
        }

        public IQueryable<WyposazenieSali> getAllEquipment() {
            Concrete<WyposazenieSali> concreteWyposazenieSali = new Concrete<WyposazenieSali>();
            return concreteWyposazenieSali
                .interfaceRepository;
        }

        public IQueryable<WyposazenieSali> getEquipmentByClassroom(int id) {
            Concrete<WyposazenieSali> concreteWS = new Concrete<WyposazenieSali>();
            Concrete<Sala_WyposazenieSali> concreteS_WS = new Concrete<Sala_WyposazenieSali>();
            int[] wsIds = concreteS_WS
                .interfaceRepository
                .Where(ws => ws.IdSala == id)
                .Select(ws => ws.IdWyposazenieSali)
                .ToArray();
            return concreteWS
                .interfaceRepository
                .Where(ws => wsIds.Contains(ws.Id));
        }

        public void removeBuilding(int buildingId) {
            Concrete<Budynek> concreteBudynek = new Concrete<Budynek>();
            concreteBudynek
                .Delete(buildingId);
        }

        public void removeClassroom(int id) {
            Concrete<Sala> concreteSala = new Concrete<Sala>();
            Concrete<Sala_WyposazenieSali> concreteS_WS = new Concrete<Sala_WyposazenieSali>();
            int [] classroomEquipment = concreteS_WS
                .interfaceRepository
                .Where(ws => ws.IdSala == id)
                .Select(ws => ws.Id)
                .ToArray();
            foreach(int equipmentsId in classroomEquipment) {
                concreteS_WS.Delete(equipmentsId);
            }
            concreteSala.Delete(id);
        }

        public void removeEquipmentFromClassroom(int classroomId, WyposazenieSali wyposazenieSali) {
            Concrete<Sala_WyposazenieSali> concreteS_WS = new Concrete<Sala_WyposazenieSali>();
            int [] relationsToRemove = concreteS_WS
                .interfaceRepository
                .Where(p => p.IdSala == classroomId && p.IdWyposazenieSali == wyposazenieSali.Id)
                .Select(p => p.Id)
                .ToArray();
            foreach(int relationId in relationsToRemove) {
                concreteS_WS.Delete(relationId);
            }
        }
    }
}
