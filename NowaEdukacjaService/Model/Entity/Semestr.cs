using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Semestr : Entity
    {
        [Column("rok"), Required]
        public Int32 Rok { get; set; }

        [Column("typ"), Required]
        public TypSemestru Typ { get; set; }

        //Wyliczane z Przedmiot
        //private Przedmiot[] przedmioty;

        //Wyliczane z Zapisy
        //private Zapisy[] zapisy;

    }

}
