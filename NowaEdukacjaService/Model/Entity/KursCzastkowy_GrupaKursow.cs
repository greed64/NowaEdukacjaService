using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class KursCzastkowy_GrupaKursow : Entity{

        //Blad przy migracji
        //[Column("kurs"), Required]
        //public int IdKurs { get; set; }
        //[ForeignKey("IdKurs")]
        //protected virtual Kurs kurs { get; set; }

        [Column("grupaKursow"), Required]
        public int IdGrupaKursow { get; set; }
        [ForeignKey("IdGrupaKursow")]
        protected virtual GrupaKursow grupaKursow { get; set; }
    }

}
