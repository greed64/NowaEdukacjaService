using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class JednostkaOrganizacyjna : Entity {

        [Column("skrot", TypeName = "nvarchar"), StringLength(20)]
        public String Skrot { get; set; }

        [Column("nazwa", TypeName = "nvarchar"), StringLength(120)]
        public String Nazwa { get; set; }

        //Wyliczane z Pracownik
        //private Pracownik[] pracownicy;
        //Wyliczane z Przedmiot
        //private Przedmiot[] podlegaPod;

    }

}
