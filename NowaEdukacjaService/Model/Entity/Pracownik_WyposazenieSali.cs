using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Pracownik_WyposazenieSali : Entity{

        [Column("pracownik"), Required]
        public int IdPracownik { get; set; }
        [ForeignKey("IdPracownik")]
        protected virtual Pracownik pracownik { get; set; }

        [Column("wyposazenieSali"), Required]
        public int IdWyposazenieSali { get; set; }
        [ForeignKey("IdWyposazenieSali")]
        protected virtual WyposazenieSali wyposazenieSali { get; set; }
    }

}
