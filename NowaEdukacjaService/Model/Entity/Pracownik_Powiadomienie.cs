using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Pracownik_Powiadomienie : Entity{

        //Blad migracji
        //[Column("pracownik"), Required]
        //public int IdPracownik { get; set; }
        //[ForeignKey("IdPracownik")]
        //protected virtual Pracownik pracownik { get; set; }

        [Column("powiadomienie"), Required]
        public int IdPowiadomienie { get; set; }
        [ForeignKey("IdPowiadomienie")]
        protected virtual Powiadomienie powiadomienie { get; set; }
    }

}
