using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Przedmiot : Entity {
        [Column("nazwa", TypeName = "nvarchar"), StringLength(120)]
        public String nazwa { get; set; }

        [Column("semestr"), Required]
        public int IdSemestr { get; set; }
        [ForeignKey("IdSemestr")]
        protected virtual Semestr semestr { get; set; }

        [Column("jednostkaOrganizacyjna"), Required]
        public int IdJednostkaOrganizacyjna { get; set; }
        [ForeignKey("IdJednostkaOrganizacyjna")]
        protected virtual JednostkaOrganizacyjna jednostkaOrganizacyjna { get; set; }

        //Wyliczane z Kurs
        //public Kurs[] realizuje2;


        [Column("planStudiow"), Required]
        public int IdPlanStudiow { get; set; }
        [ForeignKey("IdPlanStudiow")]
        protected virtual PlanStudiow planStudiow { get; set; }

        //[Column("pracownikAutor")]
        //public int IdPracownik { get; set; }
        //[ForeignKey("IdPracownik")]
        //protected virtual Pracownik pracownikAutor { get; set; }

	}

}
