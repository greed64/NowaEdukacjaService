using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class GrupaZajeciowa : Entity {

        [Column("kod", TypeName = "nvarchar"), StringLength(15), Required]
        public String Kod { get; set; }

        [Column("kurs"), Required]
        public int IdKurs { get; set; }
        [ForeignKey("IdKurs")]
        protected virtual Kurs kurs { get; set; }

        //Wyliczane z Zajecia
        //private Zajecia[] zajecia;

        //To samo co dla pracownikDydaktyczny2
        //[Column("pracownikDydaktyczny1")]
        //public int IdPracownikDydaktyczny1 { get; set; }
        //[ForeignKey("IdPracownikDydaktyczny1")]
        //protected virtual PracownikDydaktyczny pracownikDydaktyczny1 { get; set; }

        //Zakomentowane bo nie chciala przejsc migracja, nawyzej bedzie tylko jeden Introducing FOREIGN KEY constraint 'FK_dbo.GrupaZajeciowas_dbo.Pracowniks_pracownikDydaktyczny1' on table 'GrupaZajeciowas' may cause cycles or multiple cascade paths.
        //[Column("pracownikDydaktyczny2")]
        //public int IdPracownikDydaktyczny2 { get; set; }
        //[ForeignKey("IdPracownikDydaktyczny2")]
        //protected virtual PracownikDydaktyczny pracownikDydaktyczny2 { get; set; }


        //Nowa tabela Zapisy_GrupaZajeciowa (relacaja wiele do wiele)
        //private Zapisy[] zapisy;

    }

}
