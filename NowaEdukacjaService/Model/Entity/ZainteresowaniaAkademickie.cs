using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class ZainteresowaniaAkademickie : Entity {

        [Column("nazwa"), Required]
        public string Nazwa { get; set; }

	}

}
