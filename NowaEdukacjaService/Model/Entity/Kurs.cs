using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class Kurs : Entity {

        [Column("egzamin"), Required]
        public Boolean Egzamin { get; set; }

        [Column("kod", TypeName = "nvarchar"), StringLength(20),Required]
        public String Kod { get; set; }

        [Column("liczbaGodzinCNPS"), Required]
        public Int32 LiczbaGodzinCNPS { get; set; }

        [Column("liczbaGodzinZZU"), Required]
        public Int32 LiczbaGodzinZZU { get; set; }

        [Column("planowanaLiczbaGrup"), Required]
        public Int32 PlanowanaLiczbaGrup { get; set; }

        [Column("przedmiot"), Required]
        public int IdPrzedmiot { get; set; }
        [ForeignKey("IdPrzedmiot")]
        protected virtual Przedmiot przedmiot { get; set; }

        private Przedmiot realizuje;

        //Nowa klasa Kurs_WyposazenieSali
        //private WyposazenieSali[] wyposazenieSali;

        //Wyliczane z grupaKursow
        //private GrupaKursow kursKoncowy;

        //Nowa klasa KursCzastkowy_GrupaKursow
        //private GrupaKursow[] kursCzastkowy;

        //Wyliczane z GrupaZajeciowa
        //private GrupaZajeciowa[] grupyZajeciowe;

        [Column("formaZajec"), Required]
        public int IdFormaZajec { get; set; }
        [ForeignKey("IdFormaZajec")]
        protected virtual FormaZajec formaZajec { get; set; }


        //Nowa klasa Kurs_Powierzenie_PracownikDydaktyczny
        //private Powierzenie[] jestPowierzonyDla2;

	}

}
