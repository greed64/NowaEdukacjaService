using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class Powiadomienie : Entity {

        [Column("dataWyslania", TypeName = "datetime")]
        public DateTime DataWyslania { get; set; }


        [Column("temat", TypeName = "nvarchar"), StringLength(100)]
        public String Temat { get; set; }


        [Column("tresc", TypeName = "nvarchar"), StringLength(255)]
        public String Tresc { get; set; }

        [Column("nadawca"), Required]
        public int IdPracownik { get; set; }
        [ForeignKey("IdPracownik")]
        protected virtual Pracownik nadawca { get; set; }

        //Nowa klasa Pracownik_Powiadomienie
        //private Pracownik[] odbiorcy;

        //Nowa klasa Raport_Powiadomienie
        //private Raport[] raport;

	}

}
