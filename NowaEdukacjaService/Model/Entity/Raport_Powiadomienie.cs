using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Raport_Powiadomienie : Entity{

        [Column("raport"), Required]
        public int IdRaport { get; set; }
        [ForeignKey("IdRaport")]
        protected virtual Raport raport { get; set; }

        [Column("powiadomienie"), Required]
        public int IdPowiadomienie { get; set; }
        [ForeignKey("IdPowiadomienie")]
        protected virtual Powiadomienie powiadomienie { get; set; }
    }

}
