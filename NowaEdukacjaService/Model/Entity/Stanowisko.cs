using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Stanowisko : Entity {

        [Column("nazwa", TypeName = "nvarchar"), StringLength(80), Required]
        public String Nazwa { get; set; }

        [Column("pensum"),Required]
        public Int32 Pensum { get; set; }

        //Wyliczane z pracownik
        //private Pracownik[] pracownicy;

    }

}
