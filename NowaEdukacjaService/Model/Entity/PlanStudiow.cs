using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class PlanStudiow : Entity {

        [Column("stopien"), Required]
        public Int32 Stopien { get; set; }


        [Column("tryb"), Required]
        public Tryb Tryb { get; set; }

        [Column("jezyk"), Required]
        public Jezyk Jezyk { get; set; }

        [Column("kierunek", TypeName = "nvarchar"), StringLength(50), Required]
        public String Kierunek { get; set; }


        //Wyliczane z Przedmiot
        //private Przedmiot[] przedmioty;

        [Column("cyklKsztalcenia"), Required]
        public int IdCyklKsztalcenia { get; set; }
        [ForeignKey("IdCyklKsztalcenia")]
        protected virtual CyklKsztalcenia cyklKsztalcenia { get; set; }
        
        //Wyliczane z Zapisy
		//private Zapisy[] zapisy;

	}

}
