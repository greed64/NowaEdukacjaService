using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Zapisy : Entity {

        [Column("nazwa", TypeName = "nvarchar"), StringLength(80), Required]
        public String nazwa { get; set; }

        [Column("planStudiow"), Required]
        public int IdPlanStudiow { get; set; }
        [ForeignKey("IdPlanStudiow")]
        protected virtual PlanStudiow planStudiow { get; set; }


        [Column("semestr"), Required]
        public int IdSemestr { get; set; }
        [ForeignKey("IdSemestr")]
        protected virtual Semestr semestr { get; set; }

        //Nowa tabela Zapisy_GrupaZajeciowa (relacaja wiele do wiele)
        //private GrupaZajeciowa[] grupyZajeciowe;

        //Nowa tabela Zapisy_Student (relacaja wiele do wiele)
        //private Student[] studenci;

	}

}
