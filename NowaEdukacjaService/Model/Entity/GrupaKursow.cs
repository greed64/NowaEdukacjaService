using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class GrupaKursow : Entity {

        //Wyliczane z Kurs
        //private Kurs[] kursyCzastkowe;

        [Column("kursKoncowy"), Required]
        public int IdkursKoncowy { get; set; }
        [ForeignKey("IdkursKoncowy")]
        protected virtual Kurs kursKoncowy { get; set; }

	}

}
