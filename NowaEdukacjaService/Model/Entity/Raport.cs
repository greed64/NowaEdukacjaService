using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Raport : Entity {
        [Column("nazwa", TypeName = "nvarchar"), StringLength(50), Required]
        public String Nazwa { get; set; }

        [Column("tresc", TypeName = "nvarchar"), StringLength(50), Required]
        public String Tresc { get; set; }

        //Nowa klasa Raport_Powiadomienie
        //private Powiadomienie[] powiadomienia;

    }

}
