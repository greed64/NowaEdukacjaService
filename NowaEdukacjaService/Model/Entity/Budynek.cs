using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class Budynek : Entity {

        [Column("nazwa", TypeName = "nvarchar"), StringLength(50), Required]
        public String Nazwa { get; set; }

        //Wyliczane z Sala 
        //[Column("sale")]
        //public Sala[] Sale { get; set; }

    }

}
