using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class CyklKsztalcenia : Entity {

        [Column("rok1")]
        public Int32 Rok1 { get; set; }
        [Column("rok2")]
        public Int32 Rok2 { get; set; }

        //Wyliczane z PlanStudiow
       // private PlanStudiow[] PlanyStudiow { get; set; }

    }

}
