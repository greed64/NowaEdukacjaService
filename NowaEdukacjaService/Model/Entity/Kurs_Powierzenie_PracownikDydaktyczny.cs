using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Kurs_Powierzenie_PracownikDydaktyczny : Entity{


        [Column("kurs"), Required]
        public int IdKurs { get; set; }
        [ForeignKey("IdKurs")]
        protected virtual Kurs kurs { get; set; }

        [Column("powierzenie"), Required]
        public int IdPowierzenie { get; set; }
        [ForeignKey("IdPowierzenie")]
        protected virtual Powierzenie powierzenie { get; set; }

        //Blad przy migacji dlatego zakomentowane
        [Column("pracownikDydaktyczny"), Required]
        public int IdPracownikDydaktyczny { get; set; }
        [ForeignKey("IdPracownikDydaktyczny")]
        protected virtual PracownikDydaktyczny pracownikDydaktyczny { get; set; }

    }

}
