using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class Powierzenie : Entity {

        [Column("zaakceptowane"), Required]
        public Boolean Zaakceptowane { get; set; }

        [Column("liczbaGrupProponowana"), Required]
        public Int32 LiczbaGrupProponowana { get; set; }

        [Column("liczbaGrupZaakceptowana"), Required]
        public Int32 LiczbaGrupZaakceptowana { get; set; }

        [Column("komentarz", TypeName = "nvarchar"), StringLength(255)]
        public String Komentarz { get; set; }

        //Blad przy migracji
        //[Column("kurs"), Required]
        //public int IdKurs { get; set; }
        //[ForeignKey("IdKurs")]
        //protected virtual Kurs kurs { get; set; }


        //Blad przy migracji
        //[Column("pracownikDydaktyczny"), Required]
        //public int IdPracownikDydaktyczny { get; set; }
        //[ForeignKey("IdPracownikDydaktyczny")]
        //protected virtual PracownikDydaktyczny pracownikDydaktyczny { get; set; }

    }

}
