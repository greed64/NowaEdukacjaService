using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Kurs_WyposazenieSali : Entity{

        [Column("kurs"), Required]
        public int IdKurs { get; set; }
        [ForeignKey("IdKurs")]
        protected virtual Kurs kurs { get; set; }

        [Column("wyposazenieSali"), Required]
        public int IdWyposazenieSali { get; set; }
        [ForeignKey("IdWyposazenieSali")]
        protected virtual WyposazenieSali wyposazenieSali { get; set; }
    }

}
