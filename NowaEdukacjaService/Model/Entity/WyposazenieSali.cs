using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class WyposazenieSali : Entity {

        [Column("nazwa", TypeName = "nvarchar"), StringLength(50), Required]
        public String Nazwa { get; set; }

        [Column("identyfikator", TypeName = "nvarchar"), StringLength(50), Required]
        public String Identyfikator { get; set; }


        //Nowa tabela Sala_wyposazenieSali (relacaja wiele do wiele)
        //[Column("wyposazenieSali")]
        ///public Sala[] Sale { get; set; }

        //Nowa tabela Pracownik_wyposazenieSali (relacaja wiele do wiele)
        //[Column("odpowiedzialni")]
        //public Pracownik[] Odpowiedzialni { get; set; }

    }

}
