using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public abstract class Pracownik : Entity{

        [Column("imie", TypeName = "nvarchar"), StringLength(30), Required]
        public String Imie { get; set; }

        [Column("nazwisko", TypeName = "nvarchar"), StringLength(40), Required]
        public String Nazwisko { get; set; }

        [Column("tytulyNaukowe", TypeName = "nvarchar"), StringLength(40), Required]
        public String TytulyNaukowe { get; set; }

        [Column("login", TypeName = "nvarchar"), StringLength(20)]
        public String Login { get; set; }

        [Column("haslo", TypeName = "nvarchar"), StringLength(20)]
        public String Haslo { get; set; }

        //Wyliczane z Przedmiot
        //public Przedmiot[] przedmiotyAutorstwa;

        //Nowa tabela Pracownik_wyposazenieSali (relacaja wiele do wiele)
        //private WyposazenieSali[] wyposazeniaSali;

        //(Odkomentowano.)//Zakomentowane bo przy DB update by� b��d Introducing FOREIGN KEY constraint 'FK_dbo.Pracowniks_dbo.JednostkaOrganizacyjnas_jednostkaOrganizacyjna' on table 'Pracowniks' may cause cycles or multiple cascade paths.
        [Column("jednostkaOrganizacyjna")]
        public int? IdJednostkaOrganizacyjna { get; set; }
        [ForeignKey("IdJednostkaOrganizacyjna")]
        protected virtual JednostkaOrganizacyjna jednostkaOrganizacyjna { get; set; }

        [Column("stanowisko"), Required]
        public int IdStanowisko { get; set; }
        [ForeignKey("IdStanowisko")]
        protected virtual Stanowisko stanowisko { get; set; }

        //Wyliczane z Powiadomienie
        //public Powiadomienie[] NadawcaPowiadomienia;

        //Nowa klasa Pracownik_Powiadomienie
        //public Powiadomienie[] NadawcaPowiadomienia;

    }

}
