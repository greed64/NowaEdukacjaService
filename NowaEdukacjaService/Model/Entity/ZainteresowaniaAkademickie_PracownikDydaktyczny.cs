using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class ZainteresowaniaAkademickie_PracownikDydaktyczny : Entity{

        [Column("pracownikDydaktyczny"), Required]
        public int IdPracownikDydaktyczny { get; set; }
        [ForeignKey("IdPracownikDydaktyczny")]
        protected virtual PracownikDydaktyczny pracownikDydaktyczny { get; set; }

        [Column("zainteresowaniaAkademickie"), Required]
        public int IdZainteresowaniaAkademickie { get; set; }
        [ForeignKey("IdZainteresowaniaAkademickie")]
        protected virtual ZainteresowaniaAkademickie zainteresowaniaAkademickie { get; set; }
    }

}
