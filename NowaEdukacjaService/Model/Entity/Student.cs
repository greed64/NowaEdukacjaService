using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Student : Entity {

        [Column("indeks", TypeName = "nvarchar"), StringLength(10), Required]
        public String Indeks { get; set; }

        [Column("imie", TypeName = "nvarchar"), StringLength(20), Required]
        public String Imie { get; set; }

        [Column("nazwisko", TypeName = "nvarchar"), StringLength(30), Required]
        public String Nazwisko { get; set; }

        //Nowa klasa Zapisy_Student
        //private Zapisy[] dozwoloneZapisy;

    }

}
