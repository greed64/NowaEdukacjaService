using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Zapisy_Student : Entity{

        [Column("zapisy"), Required]
        public int IdZapisy { get; set; }
        [ForeignKey("IdZapisy")]
        protected virtual Zapisy zapisy { get; set; }

        [Column("student"), Required]
        public int IdStudent { get; set; }
        [ForeignKey("IdStudent")]
        protected virtual Student student { get; set; }
    }

}
