using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Sala : Entity{

        //[Index("IX_NumerAndBudynek", 1, IsUnique = true)]
        [Column("numer", TypeName = "nvarchar"), StringLength(6), Required]
        public String Numer { get; set; }

        [Column("typ")Required]
        public TypSali Typ { get; set; }

        [Column("liczbaMiejsc"),Required]
        //[Range(minimum: 1, maximum: 500, ErrorMessage = "Liczba miejsc musi by� wi�ksza od 0")]
        public Int32 LiczbaMiejsc { get; set; }

        ///[Index("IX_NumerAndBudynek", 2, IsUnique = true)]
        [Column("budynek"), Required]
        public int IdBudynek { get; set; }
        [ForeignKey("IdBudynek")]
        protected virtual Budynek budynek { get; set; }


        //Nowa tabela Sala_wyposazenieSali (relacaja wiele do wiele)
        //[Column("wyposazenieSali")]
        //public WyposazenieSali[] wyposazenieSali { get; set; }

        //Wyliczane z Zajecia 
        //[Column("zajecia")]
        //public Zajecia[] zajecia { get; set; }

    }

}
