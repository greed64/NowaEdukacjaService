using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class FormaZajec : Entity {

        [Column("nazwa", TypeName = "nvarchar"), StringLength(80), Required]
        public String nazwa { get; set; }

        [Column("minLiczbaMiejsc"), Required]
        public Int32 MinLiczbaMiejsc { get; set; }

        [Column("maxLiczbaMiejsc"), Required]
        public Int32 MaxLiczbaMiejsc { get; set; }

        //Wyliczane z Kurs
		//private Kurs[] kursy;

	}

}
