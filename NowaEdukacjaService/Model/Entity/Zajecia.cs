using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Zajecia : Entity {

        [Column("eLearning"), Required]
        public bool eLearning { get; set; }

        [Column("godzinaStart", TypeName = "datetime"), Required]
        public DateTime GodzinaStart { get; set; }

        [Column("godzinaStop", TypeName = "datetime"), Required]
        public DateTime GodzinaStop { get; set; }

        [Column("sala"), Required]
        public int IdSala { get; set; }
        [ForeignKey("IdSala")]
        protected virtual Sala sala { get; set; }

        [Column("grupaZajeciowa"), Required]
        public int IdGrupaZajeciowa { get; set; }
        [ForeignKey("IdGrupaZajeciowa")]
        protected virtual GrupaZajeciowa grupaZajeciowa { get; set; }

	}

}
