using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Sala_WyposazenieSali : Entity{

        [Column("sala"), Required]
        public int IdSala { get; set; }
        [ForeignKey("IdSala")]
        protected virtual Sala sala { get; set; }

        [Column("wyposazenieSali"), Required]
        public int IdWyposazenieSali { get; set; }
        [ForeignKey("IdWyposazenieSali")]
        protected virtual WyposazenieSali wyposazenieSali { get; set; }
    }

}
