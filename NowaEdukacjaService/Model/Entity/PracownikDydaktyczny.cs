using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;
namespace NowaEdukacjaService {
	public class PracownikDydaktyczny : Pracownik  {

        [Column("obnizeniePensum"),Required]
        public Int32 ObnizeniePensum { get; set; }

        [Column("pensum")]
        public Int32 Pensum { get; set; }

        //Nowa tabela ZainteresowaniaAkademickie_PracownikDydaktyczny
        //public ZainteresowaniaAkademickie[] zainteresowaniaAkademickie;

        //Wyliczane z GrupaZajeciowa
        //private GrupaZajeciowa[] grupyZajeciowe;

        //Nowa tabela Powierzenie_PracownikDydaktyczny
        //public Powierzenie[] jestPowierzonyDla;

    }

}
