using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using NowaEdukacjaService.Model.Abstract;

namespace NowaEdukacjaService {
	public class Zapisy_GrupaZajeciowa : Entity{

        //Blad migracji
        //[Column("zapisy"), Required]
        //public int IdZapisy { get; set; }
        //[ForeignKey("IdZapisy")]
        //protected virtual Zapisy zapisy { get; set; }

        [Column("grupaZajeciowa"), Required]
        public int IdGrupaZajeciowa { get; set; }
        [ForeignKey("IdGrupaZajeciowa")]
        protected virtual GrupaZajeciowa grupaZajeciowa { get; set; }
    }

}
