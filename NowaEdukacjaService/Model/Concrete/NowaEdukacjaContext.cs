﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace NowaEdukacjaService.Model.Concrete
{
    public class NowaEdukacjaContext : DbContext
    {
        public NowaEdukacjaContext() : base("name=NowaEdukacjaDBConnection") {
            this.Configuration.ProxyCreationEnabled = false;
        }

        // public NowaEdukacjaContext() : base("name=NowaEdukacjaDBLocalConnection") { }

        //Ok
        public DbSet<Budynek> Budynek { get; set; }

        //Ok
        public DbSet<Sala> Sala { get; set; }

        //Ok
        public DbSet<WyposazenieSali> WyposazenieSali { get; set; }

        //Ok
        public DbSet<PracownikDydaktyczny> PracownikDydaktyczny { get; set; }

        //Ok
        public DbSet<Kurs_Powierzenie_PracownikDydaktyczny> Kurs_Powierzenie_PracownikDydaktyczny { get; set; }

        //Ok
        public DbSet<Powierzenie> Powierzenie { get; set; }

        public DbSet<ZainteresowaniaAkademickie> ZainteresowaniaAkademickie { get; set; }

        //Ok
        public DbSet<ZainteresowaniaAkademickie_PracownikDydaktyczny> ZainteresowaniaAkademickie_PracownikDydaktyczny { get; set; }

        //Ok
        public DbSet<Pracownik> Pracownik { get; set; }

        //Ok
        public DbSet<PracownikDzialuPlanowania> PracownikDzialuPlanowania { get; set; }

        //Ok
        public DbSet<PracownikSekcjiPlanowaniaDydatkyki> PracownikSekcjiPlanowaniaDydatkyki { get; set; }

        //Ok
        public DbSet<PelnomocnikDziekanaDoSprawKierunku> PelnomocnikDziekanaDoSprawKierunku { get; set; }

        //Ok
        public DbSet<Przedmiot> Przedmiot { get; set; }

        //Ok
        public DbSet<JednostkaOrganizacyjna> JednostkaOrganizacyjna { get; set; }

        //Ok
        public DbSet<Kurs> Kurs { get; set; }

        //Ok
        public DbSet<Kurs_WyposazenieSali> Kurs_WyposazenieSali { get; set; }

        //Ok
        public DbSet<KursCzastkowy_GrupaKursow> KursCzastkowy_GrupaKursow { get; set; }

        //Ok
        public DbSet<GrupaKursow> GrupaKursow { get; set; }

        //Ok
        public DbSet<FormaZajec> FormaZajec { get; set; }

        //Ok
        public DbSet<Powiadomienie> Powiadomienie { get; set; }

        //Ok
        public DbSet<Raport_Powiadomienie> Raport_Powiadomienie { get; set; }

        //Ok
        public DbSet<Raport> Raport { get; set; }

        //Ok
        public DbSet<Pracownik_Powiadomienie> Pracownik_Powiadomienie { get; set; }

        //Ok
        public DbSet<Pracownik_WyposazenieSali> Pracownik_WyposazenieSali { get; set; }

        //Ok
        public DbSet<Zajecia> Zajecia { get; set; }

        //Ok
        public DbSet<GrupaZajeciowa> GrupaZajeciowa { get; set; }

        //Ok
        public DbSet<Zapisy> Zapisy { get; set; }

        //Ok
        public DbSet<Stanowisko> Stanowisko { get; set; }

        //Ok
        public DbSet<PlanStudiow> PlanStudiow { get; set; }

        //Ok
        public DbSet<CyklKsztalcenia> CyklKsztalcenia { get; set; }

        //Ok
        public DbSet<Semestr> Semestr { get; set; }

        //Ok
        public DbSet<Zapisy_Student> Zapisy_Student { get; set; }

        //Ok
        public DbSet<Student> Student { get; set; }

        //Ok
        public DbSet<Zapisy_GrupaZajeciowa> Zapisy_GrupaZajeciowa { get; set; }

        //Ok
        public DbSet<Sala_WyposazenieSali> Sala_WyposazenieSali { get; set; }

    }
}