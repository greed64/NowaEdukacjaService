﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NowaEdukacjaService.Model.Abstract;
using System.Data.Entity;

namespace NowaEdukacjaService.Model.Concrete
{
    public class Concrete<T> where T : Entity
    {
        private static NowaEdukacjaContext context = new NowaEdukacjaContext();
        private DbSet<T> concreteContext = context.Set<T>();

        // private DbSet<Budynek> asdasd = context.Budynek;
        // private DbSet<Budynek> asdasdsss = context.Set<Budynek>();

        public NowaEdukacjaContext getContext()
        {
            return context;
        }

        public IQueryable<T> interfaceRepository
        {
            get
            {
                return concreteContext;
            }
        }

        public T Delete(int id)
        {
            T dbEntry = concreteContext.Find(id);

            if (dbEntry != null)
            {
                concreteContext.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public int Save(T T)
        { 
           if (T.Id == 0  || T.Equals(null))
            {
                concreteContext.Add(T);
            }
            else
            {
                T dbEntry = concreteContext.Find(T.Id);
                if (dbEntry != null)
                {
                    dbEntry = T;
                }
            }
            context.SaveChanges();
            return T.Id;
        }
    }
}