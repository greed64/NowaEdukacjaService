﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NowaEdukacjaService.Model.Abstract;
using NowaEdukacjaService.Model.Concrete;

namespace NowaEdukacjaService.Model.Concrete
{
    public class BudynekRepository : IRepository<Budynek>
    {
        private NowaEdukacjaContext context = new NowaEdukacjaContext();
        public IQueryable<Budynek> interfaceRepository
        {
            get
            {
                return context.Budynek;
            }
        }

        public Budynek Delete(int id)
        {
            Budynek dbEntry = context.Budynek.Find(id);

            if (dbEntry != null)
            {
                context.Budynek.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public int Save(Budynek budynek)
        { 
           if (budynek.Id == 0  || budynek.Equals(null))
            {
                context.Budynek.Add(budynek);
            }
            else
            {
                Budynek dbEntry = context.Budynek.Find(budynek.Id);
                if (dbEntry != null)
                {
                    dbEntry = budynek;
                }
            }
            context.SaveChanges();
            return budynek.Id;
        }
    }
}