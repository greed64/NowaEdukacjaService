﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NowaEdukacjaService.Model.Concrete;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace NowaEdukacjaService.Model.Abstract
{
    public abstract class Entity
    {
        [Column("id"), Key, Required]
        public Int32 Id { get; set; }

    }
}
