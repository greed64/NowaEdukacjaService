﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NowaEdukacjaService.Model.Concrete;

namespace NowaEdukacjaService.Model.Abstract
{
    interface IRepository<T>
    {
        IQueryable<T> interfaceRepository { get; }

        int Save(T rep);

        T Delete(int id);
    }
}
