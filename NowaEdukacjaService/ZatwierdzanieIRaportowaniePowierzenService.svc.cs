﻿using NowaEdukacjaService.Model.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NowaEdukacjaService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ZatwierdzanieIRaportowaniePowierzenService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ZatwierdzanieIRaportowaniePowierzenService.svc or ZatwierdzanieIRaportowaniePowierzenService.svc.cs at the Solution Explorer and start debugging.
    public class ZatwierdzanieIRaportowaniePowierzenService : IZatwierdzanieIRaportowaniePowierzenService
    {
        public int addCommittal(int CourseId, int EmployeeId, int GroupCount)
        {
            try
            {
                Concrete<Powierzenie> concretePowierzenie = new Concrete<Powierzenie>();
                Concrete<Kurs_Powierzenie_PracownikDydaktyczny> concreteK_P_PD = new Concrete<Kurs_Powierzenie_PracownikDydaktyczny>();

                int powierzenieId = concretePowierzenie.Save(new Powierzenie
                {
                    Zaakceptowane = false,
                    LiczbaGrupProponowana = GroupCount,
                    LiczbaGrupZaakceptowana = 0
                });

                concreteK_P_PD.Save(new Kurs_Powierzenie_PracownikDydaktyczny
                {
                    IdKurs = CourseId,
                    IdPowierzenie = powierzenieId,
                    IdPracownikDydaktyczny = EmployeeId
                });

                return powierzenieId;
            }
            catch (Exception e)
            {
                Fault mf = new Fault();
                mf.Operation = "Dodaj Powierzenie";
                mf.Message = $"Podczas dodawania powierzenia wystąpił błąd o treści: {e.Message}";
                throw new FaultException<Fault>(mf);
            }
        }

        public IQueryable<FormaZajec> getAllCourseForms()
        {
            Concrete<FormaZajec> concreteFormaZajec = new Concrete<FormaZajec>();
            return concreteFormaZajec.interfaceRepository;

            throw new NotImplementedException();
        }

        public IQueryable<CyklKsztalcenia> getAllEducationCycles()
        {
            Concrete<CyklKsztalcenia> concreteCyklKsztalcenia = new Concrete<CyklKsztalcenia>();
            return concreteCyklKsztalcenia.interfaceRepository;
        }

        public IQueryable<Stanowisko> getAllJobTitles()
        {
            Concrete<Stanowisko> concreteStanowisko = new Concrete<Stanowisko>();
            return concreteStanowisko.interfaceRepository;
        }

        public IQueryable<JednostkaOrganizacyjna> getAllOrgUnits()
        {
            Concrete<JednostkaOrganizacyjna> concreteJednostkaOrganizacyjna = new Concrete<JednostkaOrganizacyjna>();
            return concreteJednostkaOrganizacyjna.interfaceRepository
                .OrderBy(j => j.Skrot)
                .ThenBy(j => j.Nazwa)
                .ThenBy(j => j.Id);
        }

        public IQueryable<Semestr> getAllSemesters()
        {
            Concrete<Semestr> concreteSemestr = new Concrete<Semestr>();
            return concreteSemestr.interfaceRepository;
        }

        public IQueryable<PlanStudiow> getAllStudyPlans()
        {
            Concrete<PlanStudiow> concretePlanStudiow = new Concrete<PlanStudiow>();
            return concretePlanStudiow.interfaceRepository;
        }

        public IQueryable<Powierzenie> getCommittalsByCourses(List<int> ids_from_courses)
        {
            Concrete<Kurs_Powierzenie_PracownikDydaktyczny> concreteK_P_PD = new Concrete<Kurs_Powierzenie_PracownikDydaktyczny>();
            var reducedK_P_PD = concreteK_P_PD.interfaceRepository
                .Where(kppd => ids_from_courses.Contains(kppd.IdKurs))
                .Select(kppd => kppd.IdPowierzenie)
                .ToList();

            Concrete<Powierzenie> concretePowierzenie = new Concrete<Powierzenie>();
            return concretePowierzenie.interfaceRepository
                .Where(p => reducedK_P_PD.Contains(p.Id));
        }

        public IQueryable<Kurs> getCoursesByCyclePlanSemester(int EducationCycleId, int StudyPlanId, int SemesterId)
        {
            Concrete<Kurs> concreteKurs = new Concrete<Kurs>();
            Concrete<Przedmiot> concretePrzedmiot = new Concrete<Przedmiot>();
            var reducedPrzedmiot = concretePrzedmiot.interfaceRepository;

            if (EducationCycleId != -1)
            {
                if (StudyPlanId != -1)
                {
                    reducedPrzedmiot = reducedPrzedmiot
                        .Where(p => p.IdPlanStudiow == StudyPlanId);
                }
                else
                {
                    Concrete<PlanStudiow> concretePlanStudiow = new Concrete<PlanStudiow>();

                    var planStudiowIds = concretePlanStudiow.interfaceRepository
                        .Where(sp => sp.IdCyklKsztalcenia == EducationCycleId)
                        .Select(sp => sp.Id)
                        .ToList();

                    reducedPrzedmiot = reducedPrzedmiot
                        .Where(p => planStudiowIds.Contains(p.IdPlanStudiow));
                }
            }
            
            if (SemesterId != -1)
            {
                reducedPrzedmiot = reducedPrzedmiot
                    .Where(p => p.IdSemestr == SemesterId);
            }

            var przedmiotIds = reducedPrzedmiot.Select(p => p.Id).ToList();

            return concreteKurs.interfaceRepository
                .Where(k => przedmiotIds.Contains(k.IdPrzedmiot));
        }

        public IQueryable<PracownikDydaktyczny> getEmployeesByCommittals(List<int> ids_from_committals)
        {
            Concrete<PracownikDydaktyczny> concretePracownikDydaktyczny = new Concrete<PracownikDydaktyczny>();
            Concrete<Kurs_Powierzenie_PracownikDydaktyczny> concreteK_P_PD = new Concrete<Kurs_Powierzenie_PracownikDydaktyczny>();

            var pracownikIds = concreteK_P_PD.interfaceRepository
                .Where(kppd => ids_from_committals.Contains(kppd.IdPowierzenie))
                .Select(kppd => kppd.IdPracownikDydaktyczny)
                .ToList();

            return concretePracownikDydaktyczny.interfaceRepository
                .Where(p => pracownikIds.Contains(p.Id));
        }

        public IQueryable<PracownikDydaktyczny> getEmployeesByNameInterestTitle(string firstName, string lastName, string interest, string title)
        {
            Concrete<PracownikDydaktyczny> concretePracownikDydaktyczny = new Concrete<PracownikDydaktyczny>();
            var reducedPracownik = concretePracownikDydaktyczny.interfaceRepository;

            if (!string.IsNullOrWhiteSpace(firstName))
            {
                reducedPracownik = reducedPracownik
                    .Where(p => p.Imie.Contains(firstName));
            }

            if (!string.IsNullOrWhiteSpace(lastName))
            {
                reducedPracownik = reducedPracownik
                    .Where(p => p.Nazwisko.Contains(lastName));
            }

            if (!string.IsNullOrWhiteSpace(interest))
            {
                Concrete<ZainteresowaniaAkademickie> concreteZainteresowaniaAkademickie = new Concrete<ZainteresowaniaAkademickie>();
                Concrete<ZainteresowaniaAkademickie_PracownikDydaktyczny> concreteZA_PD = new Concrete<ZainteresowaniaAkademickie_PracownikDydaktyczny>();

                var zainteresowaniaIds = concreteZainteresowaniaAkademickie.interfaceRepository
                    .Where(z => z.Nazwa.Contains(interest))
                    .Select(z => z.Id)
                    .ToList();

                var pracownikIds = concreteZA_PD.interfaceRepository
                    .Where(zp => zainteresowaniaIds.Contains(zp.IdZainteresowaniaAkademickie))
                    .Select(zp => zp.IdPracownikDydaktyczny)
                    .ToList();

                reducedPracownik = reducedPracownik
                    .Where(p => pracownikIds.Contains(p.Id));
            }

            if (!string.IsNullOrWhiteSpace(title))
            {
                reducedPracownik = reducedPracownik
                    .Where(p => p.TytulyNaukowe.Contains(title));
            }

            return reducedPracownik;
        }

        public IQueryable<ZainteresowaniaAkademickie> getInterestsByEmployee(int id)
        {
            Concrete<ZainteresowaniaAkademickie> concreteZainteresowaniaAkademickie = new Concrete<ZainteresowaniaAkademickie>();
            Concrete<ZainteresowaniaAkademickie_PracownikDydaktyczny> concreteZA_PD = new Concrete<ZainteresowaniaAkademickie_PracownikDydaktyczny>();

            var zainteresowaniaIds = concreteZA_PD.interfaceRepository
                .Where(zapd => zapd.IdPracownikDydaktyczny == id)
                .Select(zapd => zapd.IdZainteresowaniaAkademickie)
                .ToList();

            return concreteZainteresowaniaAkademickie.interfaceRepository
                .Where(z => zainteresowaniaIds.Contains(z.Id))
                .OrderBy(z => z.Nazwa)
                .ThenBy(z => z.Id);
        }

        public Kurs_Powierzenie_PracownikDydaktyczny getLinkByCommittal(int id)
        {
            Concrete<Kurs_Powierzenie_PracownikDydaktyczny> concreteK_P_PD = new Concrete<Kurs_Powierzenie_PracownikDydaktyczny>();
            return concreteK_P_PD.interfaceRepository
                .Where(kppd => kppd.IdPowierzenie == id)
                .FirstOrDefault();
        }

        /*public IQueryable<ZainteresowaniaAkademickie> getInterestsByEmployees(List<int> ids_from_employees)
        {
            Concrete<ZainteresowaniaAkademickie> concreteZainteresowaniaAkademickie = new Concrete<ZainteresowaniaAkademickie>();
            Concrete<ZainteresowaniaAkademickie_PracownikDydaktyczny> concreteZA_PD = new Concrete<ZainteresowaniaAkademickie_PracownikDydaktyczny>();

            return concreteZA_PD.interfaceRepository
                .Goin(concreteZainteresowaniaAkademickie.interfaceRepository,
                    b => b.IdZainteresowaniaAkademickie,
                    z => z.Id,
                    (b, z) => new { b = b, z = z })
                .Where(bz => ids_from_employees.Contains(bz.b.IdPracownikDydaktyczny))
                .Select(bz => bz.z)
                .Distinct()
                .OrderBy(z => z.Nazwa)
                .ThenBy(z => z.Id);
        }*/

        public IQueryable<Przedmiot> getSubjectsByCourses(List<int> ids_from_courses)
        {
            Concrete<Kurs> concreteKurs = new Concrete<Kurs>();
            Concrete<Przedmiot> concretePrzedmiot = new Concrete<Przedmiot>();

            var przedmiotIds = concreteKurs.interfaceRepository
                .Where(k => ids_from_courses.Contains(k.Id))
                .Select(k => k.IdPrzedmiot)
                .ToList();

            return concretePrzedmiot.interfaceRepository
                .Where(p => przedmiotIds.Contains(p.Id))
                .Distinct();
        }
    }
}
