﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NowaEdukacjaService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INowaEdukacjaService" in both code and config file together.
    [ServiceContract]
    public interface INowaEdukacjaService
    {
        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Budynek> getBudynek();

        [OperationContract]
        [FaultContract(typeof(Fault))]
        void deleteBudynek(Budynek budynek);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        void addBudynek(Budynek budynek);
    }
}
