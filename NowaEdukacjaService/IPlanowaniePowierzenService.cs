﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NowaEdukacjaService
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISerwisPlanowaniaPowierzen" in both code and config file together.
	[ServiceContract]
	public interface IPlanowaniePowierzenService
	{
        [OperationContract]
        [FaultContract(typeof(Fault))]
        Pracownik getEmployee(int employeeId);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Powierzenie> getCommitals(int employeeId);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Kurs> getCoursesByCommittals(List<int> ids_from_committals);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<Przedmiot> getSubjectsByCourses(List<int> ids_from_courses);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        IQueryable<JednostkaOrganizacyjna> getOrgUnitsBySubjects(List<int> ids_from_subjects);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        bool decideOnCommittals(List<int> committals);

        [OperationContract]
        [FaultContract(typeof(Fault))]
        bool changeExistingCommittals(List<Powierzenie> committals);
    }
}
