﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace NowaEdukacjaService
{
    [MessageContract]
    public class Fault
    {
        [MessageHeader]
        public string Operation { get; set; }
        [MessageBodyMember]
        public string Message { get; set; }
    }
}