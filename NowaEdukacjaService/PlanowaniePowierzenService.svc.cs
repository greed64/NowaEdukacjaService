﻿using NowaEdukacjaService.Model.Concrete;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace NowaEdukacjaService {
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SerwisPlanowaniaPowierzen" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SerwisPlanowaniaPowierzen.svc or SerwisPlanowaniaPowierzen.svc.cs at the Solution Explorer and start debugging.
    public class PlanowaniePowierzenService : IPlanowaniePowierzenService {
        public bool changeExistingCommittals(List<Powierzenie> committals)
        {
            try
            {
                Concrete<Powierzenie> concretePowierzenie = new Concrete<Powierzenie>();

                NowaEdukacjaContext context = concretePowierzenie.getContext();

                context.Powierzenie.AddOrUpdate(
                    p => p.Id,
                    committals.ToArray()
                );

                /*foreach (Powierzenie p in committals)
                {
                    concretePowierzenie.Save(p);//na szybko//TODO W Save() "UPDATE" jest robiony poprzez zastąpienie lokalnej zmiennej.
                }*/

                return true;
            }
            catch (Exception e)
            {
                Fault mf = new Fault();
                mf.Operation = "Propozycje zmian";
                mf.Message = $"Podczas wprowadzania zmian wystąpił błąd o treści: {e.Message}";
                throw new FaultException<Fault>(mf);
            }
        }

        public bool decideOnCommittals(List<int> committals) {
            Concrete<Powierzenie> concretePowierzenie = new Concrete<Powierzenie>();
            IQueryable<Powierzenie> acceptedCommitals = concretePowierzenie
                .interfaceRepository
                .Where(p => committals.Contains(p.Id));
            foreach(Powierzenie p in acceptedCommitals) {
                p.Zaakceptowane = true;
                concretePowierzenie.Save(p);
            }
            return true;
        }

        public IQueryable<Powierzenie> getCommitals(int employeeId) {
            Concrete<Kurs_Powierzenie_PracownikDydaktyczny> concreteK_P_PD = new Concrete<Kurs_Powierzenie_PracownikDydaktyczny>();
            Concrete<Powierzenie> concretePowierzenie = new Concrete<Powierzenie>();
            int[] powierzenieIds = concreteK_P_PD
                .interfaceRepository
                .Where(m => m.IdPracownikDydaktyczny == employeeId)
                .Select(m => m.IdPowierzenie)
                .ToArray();
            return concretePowierzenie.interfaceRepository.Where(m => powierzenieIds.Contains(m.Id));
        }

        public IQueryable<Kurs> getCoursesByCommittals(List<int> ids_from_committals) {
            Concrete<Kurs_Powierzenie_PracownikDydaktyczny> concreteK_P_PD = new Concrete<Kurs_Powierzenie_PracownikDydaktyczny>();
            Concrete<Kurs> concreteKurs = new Concrete<Kurs>();
            int[] coursesIds = concreteK_P_PD
                .interfaceRepository
                .Where(m => ids_from_committals.Contains(m.IdPowierzenie))
                .Select(m => m.IdKurs)
                .ToArray();
            return concreteKurs.interfaceRepository.Where(m => coursesIds.Contains(m.Id));
        }

        public Pracownik getEmployee(int employeeId) {
            Concrete<Pracownik> concretePracownik = new Concrete<Pracownik>();
            return concretePracownik.interfaceRepository.Where(m => m.Id == employeeId).First();
        }

        public IQueryable<JednostkaOrganizacyjna> getOrgUnitsBySubjects(List<int> ids_from_subjects) {
            Concrete<JednostkaOrganizacyjna> concreteJO = new Concrete<JednostkaOrganizacyjna>();
            Concrete<Przedmiot> concretePrzedmiot = new Concrete<Przedmiot>();
            int[] joIds = concretePrzedmiot
                .interfaceRepository
                .Where(p => ids_from_subjects.Contains(p.Id))
                .Select(p => p.IdJednostkaOrganizacyjna)
                .ToArray();
            return concreteJO.interfaceRepository.Where(m => joIds.Contains(m.Id));
        }

        public IQueryable<Przedmiot> getSubjectsByCourses(List<int> ids_from_courses) {
            Concrete<Kurs> concreteKurs = new Concrete<Kurs>();
            Concrete<Przedmiot> concretePrzedmiot = new Concrete<Przedmiot>();
            int[] przedmiotIds = concreteKurs
                .interfaceRepository
                .Where(k => ids_from_courses.Contains(k.Id))
                .Select(k => k.IdPrzedmiot)
                .ToArray();
            return concretePrzedmiot.interfaceRepository.Where(p => przedmiotIds.Contains(p.Id));
        }
    }
}
